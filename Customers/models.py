from django.db import models
import datetime
from django.contrib.auth.models import User

class Customer(models.Model):
    #Holds Customer details

    first_name = models.CharField(max_length = 30)
    last_name = models.CharField(max_length = 30)
    DoB = models.DateField(null=True)
    suspended = models.BooleanField(default = True)
    credit_available = models.DecimalField(null= True, decimal_places=2, max_digits=100)

    def get_fullname(self):
        return self.first_name + " " + self.last_name

    def __str__(self):
        return self.get_fullname() + " born on " + str(self.DoB)

    def to_dict(self):
        return{
            "first name": self.first_name,
            "last name": self.last_name,
            "Date of birth": self.DoB,
            "credit available": self.credit_available
        }

    def user_has_paid(self):
        self.suspended = False
        self.save()
    
    def user_has_not_paid(self):
        self.suspended = True
        self.save()

    def new_user(self, FirstName, LastName, DoB, Email, Password):
        Customer.first_name = FirstName
        Customer.last_name = LastName
        Customer.DoB = DoB
        CustomerFullName = Customer.get_fullname
        new_User = User.objects.create_user(username = CustomerFullName, email=Email, password=Password)

