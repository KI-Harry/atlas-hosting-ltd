from django.test import TestCase
import datetime
from django.contrib.auth.models import User
from Customers.models import Customer

# Create your tests here.
class CustomerTestCase(TestCase):
    def setUp(self):
        customer_first_name = Customer.objects.create(first_name='John')
        customer_last_name = Customer.objects.create(last_name='Doe')
        customer_DoB = Customer.objects.create(DoB='1995-01-18')
        customer_status = Customer.objects.create(suspended=True)
        customer_credit = Customer.objects.create(credit_available ='1000')

    def test_fullname(self):
        my_customer = Customer.objects.get(pk=1)
        get_firstlastname = my_customer.get_fullname()
        fullname = my_customer.first_name + " " + my_customer.last_name
        self.assertEqual(get_firstlastname, fullname)