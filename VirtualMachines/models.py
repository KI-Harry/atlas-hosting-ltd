from django.db import models
import datetime 
import decimal
from Customers.models import Customer

class VirtualMachines(models.Model):
    #Servers will have a creation date field
    #A Running start time field with an end time field
    #A field indicating whether the server can be activated or not based upon customer subscription
    #A customer foreign key
    #The start stop indicators to let the server know if the users running the server
    

    serverCreatedOn = models.DateTimeField(null=True)
    lastServerStartTime = models.DateTimeField(null=True)
    lastServerEndTime = models.DateTimeField(null=True)
    totalServerRunTime = models.FloatField(null=True)
    startPermission = models.BooleanField(default= False)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    startStopIndicator = models.BooleanField(default= False) 

    def set_permission(self):
        self.startPermission = True
        self.save()

    #Upon a server creation by the user 
    def server_startup(self):

        if self.serverCreatedOn is not None and self.startPermission == True:
            self.lastServerStartTime = datetime.datetime.now()
            self.startStopIndicator = True
            self.save()
        elif self.serverCreatedOn is not None and self.startPermission == False:
            return "Oops: you do not have permission to start the server as you have not paid for it, please choose and pay for a server plan first"
        else:
            return "Oops: It appears you've tried to start up a server when one doesn't exist, please create a server first"


    def server_stop(self):

        if self.serverCreatedOn is not None and self.lastServerStartTime is not None:
            self.lastServerEndTime = datetime.datetime.now()
            self.startStopIndicator = False
            self.save()

#The total runtime calculation needs to run on a daily basis 
#This will not run unless the Start and end times have been filled
#after calculating the difference between the start and end time
#the function adds the difference onto the total server runtime
    def calculate_total_runtime(self):
        
        if self.lastServerStartTime and self.lastServerEndTime:
            runtime_today = self.lastServerEndTime - self.lastServerStartTime
            #print(runtime_today)
            runtime_convert = runtime_today.total_seconds() / 3600
            self.totalServerRunTime = float(runtime_convert) + self.totalServerRunTime
            total_runtime = self.totalServerRunTime
            self.save()
            return decimal.Decimal(total_runtime)
        else:
            return "No Start or End times have been recorded, cannot calcuate runtime total"



