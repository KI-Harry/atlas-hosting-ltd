from django.test import TestCase

# Billings tests
from Customers.models import Customer
from VirtualMachines.models import VirtualMachines
from Billing.models import Billing

class BillingTestCase(TestCase):
    def setUp(self):
        customerTest = Customer.objects.create(first_name='John', last_name='Doe', DoB='1995-01-18', suspended=False, credit_available ='1000')
        VirtMach = VirtualMachines.objects.create(serverCreatedOn='2018-09-01', lastServerStartTime='2018-09-01 08:00', lastServerEndTime='2018-09-04 12:00', 
                                                totalServerRunTime=16.50 , startPermission=True, customer=customerTest, startStopIndicator=False)
        billing_test = Billing.objects.create(customer=customerTest, vm_price=4.20, VirtualMachine=VirtMach, amountPaid=0, amountToPay=0 )

    def test_calc_runtime(self):
        my_billing = Billing.objects.get(pk=1)
        calculate_costs = my_billing.amountDue()
        #273600 seconds is 76 hours, 76 plus the existing hours 16.50 is 92.5
        #92.5 times the rate is 388.50  
        self.assertEqual(calculate_costs, 388.500)

