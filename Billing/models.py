from django.db import models
import decimal
import datetime 
from datetime import timedelta
from Customers.models import Customer
from VirtualMachines.models import VirtualMachines

class Billing(models.Model):
    #VM prices are per hour in pounds but are stored as a decimal for the purpose of price calculations
    VM_PRICES = (
        (0, 'No Server pricing chosen'),
        (2.50, 'Basic Server 500GBs storage'),
        (4.20, 'Advanced Server 1TBs storage'),
        (10.00, 'Full server 5TBs storage'),
    )
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    vm_price = models.DecimalField(choices = VM_PRICES, default = 0, decimal_places=2, max_digits=100)
    VirtualMachine = models.ForeignKey(VirtualMachines, on_delete=models.CASCADE)
    amountPaid = models.DecimalField(null= True, decimal_places=2, max_digits=100)
    amountToPay = models.DecimalField(null= True, decimal_places=2, max_digits=100)
    paydate = models.DateTimeField(null=True)

    def checkIfPaid(self):
        if self.amountPaid is not None:
            self.customer.user_has_paid()
            self.VirtualMachine.set_permission()
            self.save()

    def amountDue(self):
        #This will calculate the customers overall bill due
        #When bill for a customer is due and the duration is a week past the billing date the customer's
        #suspension will be implemented
        #Billing will commence on the 30th of every month
        VMTotalUseTime = self.VirtualMachine.calculate_total_runtime()
        VM_usage_calc = VMTotalUseTime * self.vm_price
        self.amountToPay = VM_usage_calc + self.amountToPay
        self.paydate = datetime.date.today()
        self.save()
        return self.amountToPay
        
        if self.amountToPay > Customer.credit_available:
            return "WARNING: you don't have enough to pay for your server, please topup within 7 days to keep your server"
            daysafter = self.paydate + datetime.timedelta(days=7)
            if daysafter == datetime.date.today():
                return "You have failed to pay for the server, your account will be suspended until you have made another payment"
                self.customer.user_has_not_paid()
                self.save()
